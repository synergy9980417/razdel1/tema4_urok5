import javax.swing.text.DateFormatter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DatePeriod {
    LocalDate from=LocalDate.now();
    LocalDate to=LocalDate.now();

    //формат from и to "2023-12-31"
    DatePeriod (LocalDate from, LocalDate to){
        this.from = from;
        this.to=to;
    }


    public static LocalDate getLocalDateFromScanner(){
        Scanner scanner = new Scanner(System.in);

        int[] prsDt=new int[3];
        System.out.println("Введите год YYYY:");
        prsDt[0]=scanner.nextInt();
       if (prsDt[0]<1992) throw new MySystemExceptions();

        System.out.println("Введите месяц:");
        prsDt[1]=scanner.nextInt();

        System.out.println("Введите день месяца:");
        prsDt[2]=scanner.nextInt();

        LocalDate rezult=LocalDate.of(prsDt[0],prsDt[1],prsDt[2]);
        System.out.println("вы ввели "+rezult);
        return rezult;
    }

    public static String getStrFromLocaldate(LocalDate ld)
    {
        StringBuilder sb=new StringBuilder();
        sb.insert(0,"."+ld.getYear());
        sb.insert(0,"."+ld.getMonth());
        sb.insert(0,ld.getDayOfMonth());
        return sb.toString();
    }

    public static String getStrFromLocaldate2(LocalDate ld)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String formattedDate = ld.format(formatter);
//        System.out.println(formattedDate);
        return formattedDate;
    }



}
