import com.sun.source.tree.WhileLoopTree;
import org.w3c.dom.ls.LSOutput;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.SQLOutput;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.


//        Урок 5. Работа с датами.
//        1.
//        Выведите все даты текущего года

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!");


        LocalDate first2024Date = LocalDate.of(2024,01,01);
        LocalDate end2024Date = LocalDate.of(2024,12,31);

        for (int i=0;i<=365;i++){
            System.out.println(first2024Date.plusDays(i));
        }
        LocalDate date = LocalDate.now();

        // Получаем название месяца
        String monthName = date.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault());
        System.out.println("Месяц: " + monthName);

        // Получаем название дня недели
        String dayOfWeekName = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.getDefault());
        System.out.println("День недели: " + dayOfWeekName);


        System.out.println(LocalDate.now().getMonth());
        System.out.println(LocalDate.now().getDayOfWeek());
        System.out.println(LocalDate.now().lengthOfMonth());
//        2.
//“Нарисуйте” календарь:
//        январь
//        пн вт ср чт пт сб вс
//        1  2   3   4  5   6
//... и так далее

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!");
        String[] weekArr={"пн","вт","ср","чт","пт","сб","вс"};


        String[] prntStr = new String[7];
        String[][] dayMonthArr=new String[7][7];
        int k=0;
        int predMonth;
        for (int i=1;i<=12;i++){
            System.out.println(first2024Date.getMonth());
            for (int j=0;j<=6;j++){
                System.out.printf(weekArr[j]+"\t");
            }
            System.out.println("");
            for(k=0;k<first2024Date.lengthOfMonth();k++){
                printMonth(first2024Date.plusDays(k));
            }
            System.out.println("");
            first2024Date=first2024Date.plusMonths(1);
        }
//
//        3.
//        В 12 часов 1 января 2020 года вы вылетаете из Москвы во Владивосток, длительность
//        полета составляет 10 часов 15 минут. Вопрос в том, во сколько вы приедете во
//        Владивосток? Используйте ZonedDateTime

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!");
          ZoneId zoneId1 = ZoneId.of("Europe/Moscow");
          ZonedDateTime zonedDateTime1 = ZonedDateTime.of(LocalDateTime.of(2020,1,1,12,00), zoneId1);
          System.out.println(zonedDateTime1);

          zonedDateTime1=zonedDateTime1.plusHours(10).plusMinutes(15);
        System.out.println("Время прилёта по часам с Московским временем "+zonedDateTime1);

          ZoneId zoneId2 = ZoneId.of("Asia/Vladivostok");

          ZonedDateTime zonedDateTime2 = zonedDateTime1.withZoneSameInstant(ZoneId.of("Asia/Vladivostok"));
          System.out.println("местное время во Владивостоке");
          System.out.println(zonedDateTime2);
          //        4.
//        Пользователь вводит дату, вывести все числа с 1 января тог
//        о же года до этой даты

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!");
        System.out.println("Введите дату в формате гггг-мм-дд:");
        Scanner scanner = new Scanner(System.in);

        String dt=scanner.nextLine();

        String[] prsDt=new String[3];
        prsDt[0]=dt.substring(0,dt.indexOf("-"));
        prsDt[1]=dt.substring(5,7);

        prsDt[1]=dt.substring(prsDt[0].length()+1,dt.indexOf("-",prsDt[0].length()+1));
        prsDt[2]=dt.substring((prsDt[0].length()+prsDt[1].length()+2),dt.length());


        System.out.println(prsDt[0]+" "+prsDt[1]+" "+prsDt[2]);
        LocalDate thisDate4=LocalDate.of(Integer.parseInt(prsDt[0]), Integer.parseInt(prsDt[1]), Integer.parseInt(prsDt[2]));
        LocalDate startDate4=LocalDate.of(thisDate4.getYear(),1,1);

        for (int i=0;i<thisDate4.getDayOfYear();i++)
            System.out.println(startDate4.plusDays(i));


//        5.
//        Калькулятор полетов. Пользователь вводит, из какого часового пояса он
//        вылетает (в формате
//                Europe/
//                        Moscow,
//                Asia/
//                        Vladivostok
//                и так далее.. можно
//        нагуглить для каждого города), во сколько, сколько будет лететь. Напишите
//        местное время при
//                лета

        System.out.println("!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!");
        String[] zoneId5={"Europe/Moscow","Asia/Vladivostok","Pacific/Pago_Pago"};
        for (int i=0;i<zoneId5.length;i++){
            System.out.println("("+(i+1)+")"+zoneId5[i]);
        }
        System.out.println("введите номер зоны, отправления:");
        int userZoneFrom=scanner.nextInt()-1;
        System.out.println("введите номер зоны, прибытия:");
        int userZoneTo=scanner.nextInt()-1;

        System.out.println("Введите часы вылета в формате(24)");
        int userZoneFromHour=scanner.nextInt();
        System.out.println("Введите минуты:");
        int userZoneFromMin=scanner.nextInt();
        System.out.println("Сколько часов полёта:");
        int userZoneTimeFlyHour=scanner.nextInt();
        System.out.println("Сколько минут:");
        int userZoneTimeFlyMin=scanner.nextInt();


        LocalDateTime DateTimeFrom = LocalDateTime.of(LocalDate.now().getYear(),LocalDate.now().getMonth(),LocalDate.now().getDayOfMonth(),userZoneFromHour,userZoneFromMin);

        System.out.println("Время вылета "+DateTimeFrom);
        LocalDateTime DateTimeFromPlusFlyTime=DateTimeFrom.plusHours(userZoneTimeFlyHour).plusMinutes(userZoneTimeFlyMin);

        ZonedDateTime zoneDateTimeFromPlusFlyTime = ZonedDateTime.of(DateTimeFromPlusFlyTime, ZoneId.of(zoneId5[userZoneFrom]));
        System.out.println("Время прилёта по времени из временной зоны вылета "+zoneDateTimeFromPlusFlyTime);
        ZonedDateTime zonedDateTimeTo5 = zoneDateTimeFromPlusFlyTime.withZoneSameInstant(ZoneId.of(zoneId5[userZoneTo]));
        System.out.println("Время прилёта по времени во временой зоне прилёта "+zonedDateTimeTo5);

//        6.
//        Реализуйте класс DatePeriod, в котором будет две LocalDate. Переделайте
//        анализатор курса валют, что б он на вход принимал DatePeriod



        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!6!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Введите две даты с какой по какую показать курс валют");
        System.out.println("С какой даты?");
        LocalDate fromDate=DatePeriod.getLocalDateFromScanner();
        System.out.println("По какую дату?");
        LocalDate toDate=DatePeriod.getLocalDateFromScanner();
        LocalDate tempDate;

        if (!fromDate.isBefore(toDate)){
            tempDate=fromDate;
            fromDate=toDate;
            toDate=tempDate;
        }


        DatePeriod newPeriod=new DatePeriod(fromDate,toDate);

        String from=DatePeriod.getStrFromLocaldate2(newPeriod.from);
        String to=DatePeriod.getStrFromLocaldate2(newPeriod.to);
        String parsString6=downloadWeb("http://www.cbr.ru/currency_base/dynamics/?UniDbQuery.Posted=True&UniDbQuery.so=1&UniDbQuery.mode=1&UniDbQuery.date_req1=&UniDbQuery.date_req2=&UniDbQuery.VAL_NM_RQ=R01235&UniDbQuery.From="+from+"&UniDbQuery.To="+to);

        tempDate=fromDate;
        String temp;
        while (!tempDate.isEqual(toDate)){
            temp=DatePeriod.getStrFromLocaldate2(tempDate);
            String currentValue="<td>"+temp+"</td>        <td>1</td>        <td>";
            int Start = parsString6.lastIndexOf(currentValue);
            String kurs=parsString6.substring((Start+currentValue.length()),(tempDate.getYear()>1997)?(Start+currentValue.length()+7):(Start+currentValue.length()+10));
            System.out.println(temp+" = (" + kurs + ")");
            tempDate=tempDate.plusDays(1);
        }



//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполне
//        но более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получ
//        ил оценку не
//        менее 3 баллов



    }

    public static void printMonth(LocalDate day){
        if (day.getDayOfMonth()==1 && day.getDayOfWeek().getValue()!=1) {
            for (int j = 0; j < day.getDayOfWeek().getValue()-1; j++) {
                System.out.printf("\t");
            }
            if (day.getDayOfWeek().getValue()<7)
                System.out.printf(day.getDayOfMonth()+"\t");
            else System.out.println(day.getDayOfMonth()+"\t");
        }
        else
        if (day.getDayOfWeek().getValue()<7)
        System.out.printf(day.getDayOfMonth()+"\t");
        else System.out.println(day.getDayOfMonth()+"\t");
    }

    public static String downloadWeb(String urlStr) {
        StringBuilder result = new StringBuilder();
        String line;
        try {
            URL url = new URL(urlStr);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line = reader.readLine()) != null) result.append(line);
            reader.close();
        } catch (Exception e) {
// ...
            System.out.println(e.getMessage());
        }

        return result.toString();
    }




}